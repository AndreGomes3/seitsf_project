addpath('../data');

station_readings = readtable('station_readings.csv');
energy_costs = readtable('energy_cost_periods.csv');
station_arrivals = readtable('station_arrivals.csv');

station = 481;
select = (station_readings.station_id == station);
readings = station_readings.reading(select);
energy_used = readings(4:6) - readings(1:3);
total_energy_used = sum(energy_used);
energy_per_day = total_energy_used/...
    days(station_readings.reading_t(7)-station_readings.reading_t(1));
money_per_day = energy_per_day * energy_costs.cost_per_kwh(4);
money_per_month = money_per_day * 30;

select = (station_arrivals.arrival_station == station...
    & string(station_arrivals.bike_type) == 'E'...
    & station_arrivals.arrival_time < station_readings.reading_t(7)...
    & station_arrivals.arrival_time > station_readings.reading_t(1));

[counts,bins] = histcounts(station_arrivals.arrival_time(select),'BinMethod','hour');

n_rides = sum(counts);
energy_per_ride = total_energy_used/n_rides;

% figure
% hold on
% plot(bins(1:end-1),counts)
% yyaxis right
% plot(bins(1:end-1),counts*energy_per_ride)%power

t_rides = datetime(zeros(n_rides,1),0,0);
t_rides.Format = 'dd/MM/yyyy HH:mm';
k = 1;
for i = 1:length(counts)
    dt = (bins(i+1)-bins(i))/counts(i);
    t_rides(k:k+counts(i)-1) = bins(i):dt:bins(i+1)-dt;
    k = k + counts(i);
end

charge_power = 0.220;%[kw]
t_axis = bins(1):minutes(60):bins(end);
power = zeros(1,length(t_axis));
for i = 1:length(power)
    power(i) = power(i) + charge_power * ...
        sum(t_rides <= t_axis(i) & t_rides >= (t_axis(i) - minutes(energy_per_ride/charge_power*60) ) );
end
power = power/sum(power)*total_energy_used;
stairs(t_axis,power)
